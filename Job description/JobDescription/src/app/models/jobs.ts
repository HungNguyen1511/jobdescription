export class Jobs
{
    _id : string
    image: string;
    description?: string;
    title: string;
    money: number;
    listTag : Tag[];
    createDate : string;
    status: string;
}

export class Tag
{
    id: string;
    title: string
    description: string;
}