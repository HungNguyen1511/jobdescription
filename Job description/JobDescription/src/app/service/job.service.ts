import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Jobs } from '../models/jobs';

@Injectable({
  providedIn: 'root'
})
export class JobService {
  constructor(private http: HttpClient) { }

  getJob(): Observable<any>{
    return this.http.get('http://localhost:3000/jobs');
  }
  getJobById(id) : Observable<any>{
    return this.http.get('http://localhost:3000/jobs/' + id);
  }
  addNewJob(job : Jobs) : Observable<any>{
    return this.http.post('http://localhost:3000/jobs', job);
  }
  deleteJob (id) :Observable<any>{
    return this.http.delete('http://localhost:3000/jobs/' + id);
  }
  editJob (id, customer : any) :Observable<any>{
    return this.http.put('http://localhost:3000/jobs/' + id,customer);
  }
  searchByName (name: string) : Observable<any>{
    return this.http.get('http://localhost:3000/jobs/' + name);
  }

}
