import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobDescriptionComponent } from '../app/component/job-description/job-description.component';

const routes: Routes = [
  {
    path: "jobs",
    loadChildren: () => import("./module/job.module").then((m) => m.JobDesciptionModule)
  },
  {path: '', redirectTo: 'jobs',pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }