import { from } from 'rxjs';
import {Directive, ElementRef, HostListener, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import { Template } from '@angular/compiler/src/render3/r3_ast';

@Directive({
    selector: '[appUnless]'
})

export class UnlessDirective {
    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef) {}
    
    @Input() set appUnless(condition: boolean){
        if (!condition)
        {
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else if (condition){
            this.viewContainer.clear();
        }
    }
}
