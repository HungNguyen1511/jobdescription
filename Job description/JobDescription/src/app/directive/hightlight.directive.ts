import { from } from 'rxjs';
import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';

@Directive({
    selector: '[appHighlight]'
})

export class HighlightDirective implements OnInit {

    @Input() highlightColor: string;

    constructor(private el: ElementRef){
    }

    ngOnInit(): void {
        console.log(this.highlightColor);
        this.el.nativeElement.style.backgroundColor  = this.highlightColor;
    }

    @HostListener ('mouseenter') onMouseEnter() {
        this.el.nativeElement.style.backgroundColor  = this.highlightColor;
    }

    @HostListener ('mouseleave') onMouseLeave() {
        this.el.nativeElement.style.backgroundColor  = "red";
    }

    @HostListener ('dblclick') onDblClick() {
        this.el.nativeElement.style.backgroundColor  = "dark";
    }
}