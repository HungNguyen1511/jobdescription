import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightDirective } from './hightlight.directive';
import { BrowserModule } from '@angular/platform-browser';
import {UnlessDirective} from './unless.directive';
  import { from } from 'rxjs';

const DIRECTIVE = [
  HighlightDirective,
  UnlessDirective
]

@NgModule({
  declarations: [
    ...DIRECTIVE,
  ],
  imports: [
    BrowserModule
  ],
  exports: [
    ...DIRECTIVE
  ],
})
export class DirectiveModule { }
