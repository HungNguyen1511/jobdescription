import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDescriptionAddComponent } from './job-description-add.component';

describe('JobDescriptionAddComponent', () => {
  let component: JobDescriptionAddComponent;
  let fixture: ComponentFixture<JobDescriptionAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobDescriptionAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobDescriptionAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
