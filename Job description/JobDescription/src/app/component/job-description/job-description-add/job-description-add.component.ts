import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Jobs } from '../../../models/jobs';
import { JobService } from '../../../service/job.service';


@Component({
  selector: 'app-job-description-add',
  templateUrl: './job-description-add.component.html',
  styleUrls: ['./job-description-add.component.scss']
})
export class JobDescriptionAddComponent implements OnInit {

  job: Jobs = new Jobs;
  constructor(
    public dialogRef: MatDialogRef<JobDescriptionAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data ,
    private jobService : JobService,
    private toastrService: ToastrService
  ) { }
  
  ngOnInit(): void {
  }

  public formgroup : FormGroup;
  
  public gioiTinh : boolean = true;
  close(){
    this.dialogRef.close();
  }

  save(form){
    if (form.valid){
      this.jobService.addNewJob(this.job).subscribe(
        data => {
          this.toastrService.success("Them moi khach hang thanh cong");
          this.dialogRef.close(true);
        },
        error =>{
          console.log(error);
        }
      )
    }
    else{
      console.log('invalid form');
    }
  }
}
