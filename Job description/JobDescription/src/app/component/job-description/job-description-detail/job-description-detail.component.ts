import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Jobs } from '../../../models/jobs';
import { JobService } from '../../../service/job.service';

@Component({
  selector: 'app-job-description-detail',
  templateUrl: './job-description-detail.component.html',
  styleUrls: ['./job-description-detail.component.scss']
})
export class JobDescriptionDetailComponent implements OnInit {
  public job : Jobs;

  constructor(
    public dialogRef: MatDialogRef<JobDescriptionDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data ,
    private jobService : JobService,
    private toastrService: ToastrService

  ) { }
  jobClick : Jobs;
  ngOnInit(): void {
    console.log(this.data.id);
    this.jobService.getJobById(this.data.id).subscribe(
      success => {
        console.log(success);
        this.job = success;
      },
      error => {
        console.log(error);
      }
    )
  } 

  close(){
    this.dialogRef.close(false);
  }
 

}
