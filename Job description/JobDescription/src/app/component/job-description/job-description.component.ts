import { Component, OnInit } from '@angular/core';
import { JobDescriptionDetailComponent } from './job-description-detail/job-description-detail.component';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { JobDescriptionAddComponent } from './job-description-add/job-description-add.component';
import { Router } from '@angular/router';
import { JobService } from '../../service/job.service';
import { JobDescriptionUpdateComponent } from './job-description-update/job-description-update.component';
import { Jobs } from '../../models/jobs';
import { JobDescriptionDeleteComponent } from './job-description-delete/job-description-delete.component';
@Component({
  selector: 'app-job-description',
  templateUrl: './job-description.component.html',
  styleUrls: ['./job-description.component.scss']
})
export class JobDescriptionComponent implements OnInit {

  numberJobDescription : number;
  public job : Jobs;
  listJob: Jobs[] = []
  listResult: Jobs[] = [];
  public searchString : string;
  constructor(
    private router: Router,
    public dialog: MatDialog,
    public jobService: JobService
  ) { }
  ngOnInit(): void {
    this.listJob = [
      {
        _id : "string",
        image: "https://itviec.com/rails/active_storage/representations/proxy/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBMXgzRXc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--9747b4e82941b47df75157048a404fc195fd1a40/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RTNKbGMybDZaVjloYm1SZmNHRmtXd2RwUm1sRyIsImV4cCI6bnVsbCwicHVyIjo_idmFyaWF0aW9uIn19--e48ed52d99e56a5ff9747052d860ea1b318b2d76/logo.jpg",
        title: "Kiem thu",
        money: 10,
        listTag: [],
        createDate : "2020/02/02 15:20",
        status: "string",
      },
      {
        _id : "string1",
        image: "https://itviec.com/rails/active_storage/representations/proxy/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBN0tmSGc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--9cc178352e0da23be09ed4f931ac265da21c3f56/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2RTNKbGMybDZaVjloYm1SZmNHRmtXd2RwUm1sRyIsImV4cCI6bnVsbCwicHVyIjo_idmFyaWF0aW9uIn19--d55b6722f71e82d147ad94b8445be27797820f9f/Black%20(3).png",
        title: ".Net",
        money: 10,
        listTag: [],
        createDate : "2020/02/02 15:20",
        status: "string1",
      },
      {
        _id : "string2",
        image: "https://itviec.com/rails/active_storage/representations/proxy/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBM3g1SHc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--c535e18eca3ff84fd6857746a6ea66ed9cb31b69/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2RTNKbGMybDZaVjloYm1SZmNHRmtXd2RwUm1sRyIsImV4cCI6bnVsbCwicHVyIjo_idmFyaWF0aW9uIn19--d55b6722f71e82d147ad94b8445be27797820f9f/logo2.png",
        title: ".Java",
        money: 10,
        listTag: [],
        createDate : "2020/02/02 15:20",
        status: "string2",
      }
    ];

    
    this.jobService.getJob().subscribe(
      data => {
        console.log(data);
        this.listJob = data;
        this.listResult = data;
        this.numberJobDescription = this.listJob.length;
      },
      error =>{
        console.log(error);
      }
    );
  }
  getDetail(id: any){
    this.jobService.getJobById(id).subscribe(
      data => {
        this.job = data;
        console.log(data);
      },
      error => {
        console.log(error);
      }
    )
  }
  showDialogDetail(event: any, item) {
    event.stopPropagation();
    const dialogRef = this.dialog.open(JobDescriptionDetailComponent,{
      panelClass: 'job-detail-dialog',
      width: "500px",
      disableClose: true,
      data:{
        title: "detail",
        id: item._id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.getList();
      }
      console.log('Dialog result: ${result}');     
    });
  }

  addCustomer(){
    this.jobService.addNewJob(this.job).subscribe(
      data => {
        console.log(data);
      },
      error =>{
        console.log(error);
      }
    )
  }

  deleteJob (event, _id){
    event.stopPropagation();
    this.jobService.deleteJob(_id).subscribe(
      data => {
        console.log(data);
        this.getList();

      },
      error =>{
        console.log(error);
      }
    )
  }

  
  getList(){
    this.jobService.getJob().subscribe(
      data => {
        this.listJob = data;
        this.listResult = data;
        this.numberJobDescription = this.listResult.length;
      },
      error => {
        console.log(error);
      }
    )
  }
  openAddJobDialog(){
    const dialogRef = this.dialog.open(JobDescriptionAddComponent, {
      panelClass: 'job-add-dialog',
      width: "500px",
      disableClose: true,
      data:{}
    });
      dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.getList();
      }
      console.log('Dialog result: ${result}');
    });
  }

  openDeleteJobDialog(event,item ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(JobDescriptionDeleteComponent, {
      panelClass: 'job-delete-dialog',
      width: "500px",
      disableClose: true,
      data:{
        title: "delete",
        id: item._id
      }
    });
      dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.getList();
      }
      console.log('Dialog result: ${result}');
    });
  }


  searchJob()
  {
    this.listResult = this.listJob.filter(job => (
      job.description.includes(this.searchString) || 
      job.money.toString().includes(this.searchString)
    ))
  }

  editJob (_id){
    this.jobService.editJob(_id, this.job).subscribe(
      data => {
        console.log(data);
      },
      error =>{
        console.log(error);
      }
    )
  }

  openEditJobDialog(event,item ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(JobDescriptionUpdateComponent,{
      panelClass: 'job-update-dialog',
      width: "500px",
      disableClose: true,
      data:{
        title: "edit",
        job: item
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.getList();
      }
      console.log('Dialog result: ${result}');     
    });
  }



}
