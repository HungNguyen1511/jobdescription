import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDescriptionUpdateComponent } from './job-description-update.component';

describe('JobDescriptionUpdateComponent', () => {
  let component: JobDescriptionUpdateComponent;
  let fixture: ComponentFixture<JobDescriptionUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobDescriptionUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobDescriptionUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
