import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { JobService } from '../../../service/job.service';

@Component({
  selector: 'app-job-description-update',
  templateUrl: './job-description-update.component.html',
  styleUrls: ['./job-description-update.component.scss']
})
export class JobDescriptionUpdateComponent implements OnInit, AfterViewInit {

  editForm: FormGroup;
  constructor(private fb: FormBuilder,
    public dialogRef: MatDialogRef<JobDescriptionUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private jobService : JobService,
    private toastrService: ToastrService) { 

    this.editForm = this.fb.group({
      title: this.fb.control("", [Validators.required,Validators.minLength(5)]),
      description: this.fb.control("", [Validators.required,Validators.minLength(5)]),
      money: this.fb.control("", [Validators.required,Validators.minLength(10)]),
      image: this.fb.control("", [Validators.required,Validators.minLength(10)]),

    })
    this.editForm.patchValue(data.job);

  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    console.log('this.data', this.data);
  }

  get title() {
    return this.editForm.controls.title;
  }
  get description(){
    return this.editForm.controls.description;
  }
  get money(){
    return this.editForm.controls.money;
  }
  get image(){
    return this.editForm.controls.image;
  }
 
  save(){
    const formValue = this.editForm.value;
    this.editForm.markAllAsTouched();
    if(this.editForm.valid){
      this.jobService.editJob(this.data.job._id, {"title": formValue.title,
                                                                "description": formValue.description,
                                                                "money":formValue.money,
                                                                "image":formValue.image}).subscribe(
        data => {
          this.toastrService.success("Cap nhat Job thanh cong");
          this.dialogRef.close(true);
        },
        error =>{
          console.log(error);
          this.toastrService.error(error);
        }
      );
    }
  }

  close(){
    this.dialogRef.close(false);
  }

}
