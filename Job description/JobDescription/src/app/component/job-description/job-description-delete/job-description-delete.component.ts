import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Jobs } from '../../../models/jobs';
import { JobService } from '../../../service/job.service';

@Component({
  selector: 'app-job-description-delete',
  templateUrl: './job-description-delete.component.html',
  styleUrls: ['./job-description-delete.component.scss']
})
export class JobDescriptionDeleteComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<JobDescriptionDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data ,
    private jobService : JobService,
    private toastrService: ToastrService) { }
    
  ngOnInit(): void {
  }
  close(){
    this.dialogRef.close();
  }
  public job : Jobs;
  save(form){
    if (form.valid){
      this.jobService.deleteJob(this.data.id).subscribe(
        data => {
          this.toastrService.success("Xóa item thành công");
          this.dialogRef.close(true);
        },
        error =>{
          console.log(error);
        }
      )
    }
    else{
      console.log('invalid form');
    }
  }
}

