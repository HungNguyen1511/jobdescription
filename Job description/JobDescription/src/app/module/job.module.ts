import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { JobDescriptionComponent } from '../component/job-description/job-description.component';
import { JobDescriptionDetailComponent } from '../component/job-description/job-description-detail/job-description-detail.component';
import { JobDescriptionUpdateComponent } from '../component/job-description/job-description-update/job-description-update.component';
import { JobDescriptionAddComponent } from '../component/job-description/job-description-add/job-description-add.component';
import { JobDescriptionDeleteComponent } from '../component/job-description/job-description-delete/job-description-delete.component';

const routes: Routes = [
  { path: '',
    children: [
      {
        path: '', component: JobDescriptionComponent,
      },
      {
        path: 'detail/:id',
        component: JobDescriptionDetailComponent
      },
    ],
  },
];

@NgModule({
declarations: [
    JobDescriptionComponent,
    JobDescriptionDetailComponent,
    JobDescriptionAddComponent,
    JobDescriptionUpdateComponent,
    JobDescriptionDeleteComponent
    ],
  imports: [RouterModule.forChild(routes),CommonModule,MaterialModule,FormsModule,ReactiveFormsModule ],
  exports: [RouterModule]
})
export class JobDesciptionModule { }